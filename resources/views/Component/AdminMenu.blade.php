<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="/admin/home">GoHoliday</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/admin/home">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/info">Personal Info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/bookings">Bookings</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/addroom">Create Room</a>
            </li>
            <li class="nav-item">
                    <a class="nav-link" href="/admin/roomlist">Roomlist</a>
                </li>

        </ul>
        <form class="form-inline my-2 my-lg-0" action="/admin/adminlogout" method="POST">
            @csrf

            <button type="Submit" class="btn btn-primary">Logout</button>
        </form>




    </div>
</nav>
