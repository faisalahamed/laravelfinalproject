<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="/root/home">GoHoliday</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/root/home">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/root/info">Personal Info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/root/addhotel">Add Hotel</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/root/adduser">Add User</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/root/hotellist">Hotel list</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/root/log">Log</a>
            </li>

        </ul>
        <form class="form-inline my-2 my-lg-0" action="/root/logout" method="POST">
            @csrf

            <button type="Submit" class="btn btn-primary">Logout</button>
        </form>




    </div>
</nav>
