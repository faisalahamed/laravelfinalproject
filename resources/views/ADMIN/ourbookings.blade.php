@extends('layouts.AdminLayout')

@section('content')
<table class="table-custom-bordered  table-bordered col-12">
        <tr>
            <th>Reservation Number</th>
            <th>Room Id</th>
            <th>Check In</th>
            <th>Check in Status </th>
            <th>Check Out</th>
            <th>Check Out Status </th>
            <th>Customer Name </th>

        </tr>
        @foreach ($booking as $booking)
        <tr>
                <td>  {{$booking->reservation_number}}   </td>
                <td>  {{$booking->roomid}} </td>
                <td>{{$booking->check_in_time}}  </td>
                <td> {{$booking->check_in_status}} </td>
                <td> {{$booking->check_out_time}}  </td>
                <td>

                        <form action="/admin/checkout" method="POST" class="form-group p-2" style="margin: auto; width: 20%;">
                                @csrf

                                    <input type="text" value="{{$booking->roomid}}" name="checkout" hidden>
                                    <button type="submit"   class="btn btn-primary">checkout</button>
                            </form>



                </td>
                <td>{{$booking->name}}  </td>

        </tr>
        @endforeach


    </table>
@endsection
