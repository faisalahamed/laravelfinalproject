@extends('layouts.AdminLayout')

@section('content')
<strong>Name: {{ Auth::user()->name }}</strong>
<br>
<strong>Email: {{ Auth::user()->email }}</strong>
<br>
<strong>ID: {{ Auth::user()->id }}</strong>
<br>
<strong>Hotel ID: {{ Auth::user()->hotel_id }}</strong>
@endsection
