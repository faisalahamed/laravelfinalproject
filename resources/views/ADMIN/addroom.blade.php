@extends('layouts.AdminLayout')

@section('content')
<div class="container col-md-4">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif
    <form method="post" action="/admin/addroom" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <fieldset>
                <legend>Create Room</legend>
                <div class="form-group">
                        <label>Room Number</label>
                        <input type="text" name="roomnumber" class="form-control" value="" />
                </div>
                <div class="form-group">
                        <label>Price</label>
                        <input type="text" name="price" class="form-control" value="" />
                </div>
                <div class="form-group">
                        <label>Facility</label>
                        <label class=" custom-control checkbox-inline">
                            <input name='ac' type="checkbox" value="1">AC
                        </label>
                        <label class=" custom-control checkbox-inline">
                            <input name='fridge' type="checkbox" value="1">Fridge
                        </label>
                        <label class="custom-control checkbox-inline">
                            <input name='tv' type="checkbox" value="1">TV
                        </label>
                        <label class="custom-control checkbox-inline">
                                <input name='childcare' type="checkbox" value="1">childcare
                            </label>
                </div>
                <div class="form-group">
                        <label>Room Type</label>
                        <div class="custom-control custom-radio">
                          <input type="radio" id="customRadio1" value="single" name="customRadio" class="custom-control-input" checked="">
                          <label class="custom-control-label" for="customRadio1">Single</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input type="radio" id="customRadio2" name="customRadio" value="double" class="custom-control-input">
                          <label class="custom-control-label" for="customRadio2">Double</label>
                        </div>
                        <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio3" name="customRadio" value="family" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio3">Family</label>
                        </div>
                        <div class="form-group p-2">
                        <label for="exampleInputFile">Room Image</label>
                        <input type="file" name="image"  class="form-control-file" id="image">
                        </div>
                </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
