@extends('layouts.AdminLayout')

@section('content')


@foreach($room as $room)
        <div class="container">
            <table class="table-custom-bordered table-striped table-bordered col-md-9 m-2">
                <tr>
                    <th>Hotel Image</th>
                    <th>Hotel Details</th>
                    <th>Room Details</th>
                    <th>Action</th>
                </tr>

                <td>
                    <img style="height: 250px; width:250px"  class="img-fluid img-thumbnail "  src="/uploads/{{$room['img']}} ">
                </td>
                <td>
                    <table class="table table-condensed">
                        <tr>
                            <td >Room Number: {{$room['roomnumber']}}</td>
                        </tr>
                        <tr>
                            <td >Room Type: {{$room['room_type']}}</td>
                        </tr>
                        <tr>
                            <td >Price:{{$room['price']}} BDT</td>
                        </tr>

                    </table>
                </td>
                {{--Room Details--}}
                <td>
                    <table class=" table table-condensed">
                        <tr>
                        <td >AC: @if($room['ac']==1)<strong>{{'Available'}}</strong> @else {{'Not Available'}} @endif</td>
                        </tr>
                        <tr>
                            <td >Fridge: @if($room['fridge']==1)<strong>{{'Available'}}</strong> @else {{'Not Available'}} @endif</td>
                        </tr>
                        <tr>
                            <td >TV: @if($room['tv']==1)<strong>{{'Available'}}</strong> @else {{'Not Available'}} @endif</td>
                        </tr>
                        <tr>
                            <td >Wifi: Not Available</td>
                        </tr>


                    </table>

                </td>

                {{--Action--}}
                <td>
                    <table class=" table table-condensed">
                        <tr>
                            <td ><button type="button" class="btn btn-primary btn-lg btn-block">Edit</button></td>
                        </tr>
                        <tr>
                            <td ><button type="button" class="btn btn-success btn-lg btn-block">Remove</button></td>
                        </tr>

                    </table>
                </td>
            </table>
        </div>
    @endforeach












@endsection
