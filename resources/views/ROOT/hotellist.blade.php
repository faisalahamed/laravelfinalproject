@extends('layouts.rootlayout')

@section('content')
    @foreach($hotel as $hotel)
        <div class="container">
            <table class="table-custom-bordered table-striped table-bordered col-md-9 m-2">
                <tr>
                    <th>Hotel Image</th>
                    <th>Hotel Details</th>
                    <th>Room Details</th>
                    <th>Action</th>
                </tr>

                <td>
                    <img style="height: 250px; width:250px"  class="img-fluid img-thumbnail "  src="/uploads/{{$hotel['img']}} ">
                </td>
                <td>
                    <table class="table table-condensed">
                        <tr>
                            <td >{{$hotel['hotelname']}}</td>
                        </tr>
                        <tr>
                            <td >{{$hotel['address']}}</td>
                        </tr>
                        <tr>
                            <td >{{$hotel['star']}} Star</td>
                        </tr>

                    </table>
                </td>
                {{--Room Details--}}
                <td>
                    <table class=" table table-condensed">
                        <tr>
                            <td >Room Type</td>
                        </tr>
                        <tr>
                            <td >Cost</td>
                        </tr>
                        <tr>
                            <td >Facilities</td>
                        </tr>
                        <tr>
                            <td >Facilities</td>
                        </tr>
                        <tr>
                            <td >Facilities</td>
                        </tr>

                    </table>

                </td>

                {{--Action--}}
                <td>
                    <table class=" table table-condensed">
                        <tr>
                            <td ><button type="button" class="btn btn-primary btn-lg btn-block">Edit</button></td>
                        </tr>
                        <tr>
                            <td ><button type="button" class="btn btn-success btn-lg btn-block">Delete</button></td>
                        </tr>

                    </table>
                </td>
            </table>
        </div>
    @endforeach
@endsection
