@extends('layouts.rootlayout')

@section('content')
    <div class="container col-md-4">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
    <form method="post" action="/root/adduser">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Admin Name</label>
            <input type="text" name="name" class="form-control" value="" />
        </div>

        <div class="form-group">
            <label>Enter Admin Email</label>
            <input type="text" name="email" class="form-control" value="" />
        <div class="form-group">
            <label>Set Admin Password</label>
            <input type="text" name="password" class="form-control" value="" />
        </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="phone" class="form-control"  />
                </div>
                <div class="form-group">
                        <label for="select">User for the Hotel</label>
                        <select class="form-control " name="type">
                            @foreach ($hotel as $hotel)
                            <option value="{!! $hotel->id !!}">{!! $hotel->hotelname !!}</option>
                                @endforeach
                        </select>
                    </div>
            </div>
        </div>

        <div class="form-group">
            <input type="submit" name="send" class="btn btn-info" value="Send" />
        </div>
    </form>
</div>


@endsection
