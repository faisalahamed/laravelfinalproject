@extends('layouts.rootlayout')

@section('content')

        <div class="container">
            <table class="table-custom-bordered table-striped table-bordered col-md-9 m-2">
                <tr>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>User Type</th>
                    <th>Activity</th>
                    <th>Activity Time</th>
                </tr>
                @foreach($log as $log)
               <tr>
                    <td>{{$log['user_name']}}</td>
                   <td>{{$log['email']}}</td>
                   <td>{{$log['user_type']}}</td>
                   <td>{{$log['activity']}}</td>
                   <td>{{$log['created_at']}}</td>
               </tr>
               @endforeach
            </table>
        </div>

@endsection
