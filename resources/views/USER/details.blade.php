@extends('layouts.UserLayout')
@section('content')
@foreach ($hotelAndRoom as $hotelAndRoom)
<div class="container">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

        <table class="table-custom-bordered  table-bordered col-12">
            <tr>
                <th>Room Image</th>
            </tr>
            <tr>
                    <td><img style=""  class="img-fluid img-thumbnail "  src="/uploads/{{$hotelAndRoom->roomimg}}"></td>

             </tr>

             <tr>
                    <td>
                        <table class="table table-condensed table-custom-bordered">
                                <th>Hotel Info</th>
                                <th>Room info</th>
                                <tr>
                                    <td>Hotel Name : {{$hotelAndRoom->hotelname}}</td>
                                    <td> Room No: {{$hotelAndRoom->roomnumber}}</td>
                                </tr>
                                <tr>
                                    <td>Hotel Address : {{$hotelAndRoom->address}}</td>
                                    <td> Room Type: {{$hotelAndRoom->room_type}} </td>
                                </tr>
                                <tr>
                                    <td>Location:{{$hotelAndRoom->city}}</td>
                                    <td> Price: {{$hotelAndRoom->price}} /Day</td>
                                </tr>
                                <tr>
                                    <td>Email : {{$hotelAndRoom->email}}</td>
                                    <td> Facilities: {{$hotelAndRoom->ac==1?'AC ':" "}}{{$hotelAndRoom->fridge==1?'Fridge ':" "}}{{$hotelAndRoom->tv==1?'Tv ':" "}}</td>
                                </tr>
                                <tr>
                                    <td>Phone: {{$hotelAndRoom->phone}}</td>
                                    <td> Childcare: {{$hotelAndRoom->childcare?'yes':'no'}}</td>
                                </tr>
                                <tr>
                                    <td>Star: {{$hotelAndRoom->star}}</td>
                                    <td></td>
                                </tr>


                        </table>
                    </td>
            </tr>
            <tr>{{--serch table--}}
                <td>
                        <form action="" method="POST" class="form-group p-2" style="margin: auto; width: 20%;">
                            @csrf
                                <div class="form-group">
                                    <label for="checkin">Check in</label>
                                    <input class="form-control" id="checkin" name="check_in_date" type="date" id="date">
                                </div>
                                <input name="roomnumber" type="hidden" value= {{$hotelAndRoom->roomnumber}}>
                                <div class="form-group">
                                    <label for="checkout">Check Out</label>
                                    <input class="form-control" id="checkout" name="check_out_date" type="date" id="date">
                                </div>
                                <button type="submit"  onclick="myfunc()" class="btn btn-primary">Submit</button>
                        </form>
                           <a href="/user/reserve" class="btn btn-primary btn-lg col-md-auto m-4 ">Back</button>

                </td>

            </tr>


        </table>
</div>
@endforeach
<script>

    function myfunc(){
        var x=document.getElementById("checkin").value;
        var y=document.getElementById("checkout").value;

       if(x>=y){
           alert('wrong date input');
           event.preventDefault();
       }
       else{
        form.submit();

       }
    }


</script>
@endsection
