@extends('layouts.UserLayout')
@section('content')


<div class="container">
    <table class="table-custom-bordered  table-bordered col-12">
            <form method="get" action="/user/reserve/search" >
        <tr>
            <th>Location</th>
            <th>Check in Date</th>
            <th>Check out Date</th>
        </tr>
        <tr>
            <td>
                    <div class="form-group">

                            <input class="form-control" type="text" name="location">
                        </div>
            </td>
            <td>
                <div class="form-group">
                    <input class="form-control" id="checkin" required name="check_in_date" type="date" id="date">
                </div>
            </td>
            <td>
                <div class="form-group">
                    <input class="form-control" id="checkout" required name="check_out_date" type="date" id="date">
                </div>
            </td>
        </tr>
        <tr>
            <td>    <button type="submit"  onclick="myfunc()" class="btn btn-primary">SORT</button></td>
        </tr>
    </form>
    </table>
</div>


{{--table two hotelAndRoom hotelAndRoom   hotelAndRoom hotelAndRoom  hotelAndRoom hotelAndRoom   hotelAndRoom2 hotelAndRoom--}}
@foreach ($hotelAndRoom as $hotelAndRoom)
<div class="container ">
    <table class="table-custom-bordered  table-bordered col-12">
        <tr>
            <th>Hotel Image</th>
            <th>Hotel Details</th>
            <th>Room Image</th>
            <th>Hotel Details</th>
            <th>Action</th>

        </tr>

        {{--Hotel--}}
        <td><img style="height: 250px; width:250px"  class="img-fluid img-thumbnail "  src="/uploads/{{$hotelAndRoom->hotelimg}} "></td>
        <td>
                <table class="table table-condensed">
                        <tr>
                            <td >Hotel: {{$hotelAndRoom->hotelname}} </td>
                        </tr>
                        <tr>
                            <td >Address: {{$hotelAndRoom->address}} </td>
                        </tr>
                        <tr>
                                <td >City: {{$hotelAndRoom->city}} </td>
                            </tr>
                        <tr>
                            <td >Email: {{$hotelAndRoom->email}}</td>
                        </tr>
                        <tr>
                                <td >Star: {{$hotelAndRoom->star}}  Star</td>
                            </tr>

                    </table>
        </td>

        {{--Room--}}
        <td><img style="height: 250px; width:250px"  class="img-fluid img-thumbnail "  src="/uploads/{{$hotelAndRoom->roomimg}} "> </td>
        <td>
                <table class="table table-condensed">
                        <tr>
                            <td >Room No: {{$hotelAndRoom->roomnumber}} </td>
                        </tr>
                        <tr>
                            <td >Type: {{$hotelAndRoom->room_type}} </td>
                        </tr>
                        <tr>
                                <td ><strong>Cost: {{$hotelAndRoom->price}} /Day</strong></td>
                            </tr>
                        <tr>
                        <td >Facility: {{$hotelAndRoom->ac==1?'AC ':" "}}{{$hotelAndRoom->fridge==1?'Fridge ':" "}}{{$hotelAndRoom->tv==1?'Tv ':" "}}</td>
                        </tr>
                        <tr>
                                <td >Childcare:  {{$hotelAndRoom->childcare==1?'available ':'no'}}</td>
                                </tr>



                    </table>
        </td>
        {{--Action--}}
        <td>
            <table class=" table table-condensed">
                <tr>
                <td ><a href="/user/details/{{$hotelAndRoom->id}}" class="btn btn-primary btn-lg btn-block">Details</button></td>
                </tr>
                <tr>
                    <td ><a href="/user/details/{{$hotelAndRoom->id}}" type="button" class="btn btn-success btn-lg btn-block">Book</button></td>
                </tr>

            </table>
        </td>
    </table>
</div>
@endforeach



{{--table two hotelAndRoom2 hotelAndRoom2   hotelAndRoom2 hotelAndRoom2   hotelAndRoom2 hotelAndRoom2   hotelAndRoom2 hotelAndRoom2--}}
@if($hotelAndRoom2)
@foreach ($hotelAndRoom2 as $hotelAndRoom2)
<div class="container ">
    <table class="table-custom-bordered  table-bordered col-12">
        <tr>
            <th>Hotel Image</th>
            <th>Hotel Details</th>
            <th>Room Image</th>
            <th>Hotel Details</th>
            <th>Action</th>

        </tr>

        {{--Hotel--}}
        <td><img style="height: 250px; width:250px"  class="img-fluid img-thumbnail "  src="/uploads/{{$hotelAndRoom2->hotelimg}} "></td>
        <td>
                <table class="table table-condensed">
                        <tr>
                            <td >Hotel: {{$hotelAndRoom2->hotelname}} </td>
                        </tr>
                        <tr>
                            <td >Address: {{$hotelAndRoom2->address}} </td>
                        </tr>
                        <tr>
                                <td >City: {{$hotelAndRoom2->city}} </td>
                            </tr>
                        <tr>
                            <td >Email: {{$hotelAndRoom2->email}}</td>
                        </tr>
                        <tr>
                                <td >Star: {{$hotelAndRoom2->star}}  Star</td>
                            </tr>

                    </table>
        </td>

        {{--Room--}}
        <td><img style="height: 250px; width:250px"  class="img-fluid img-thumbnail "  src="/uploads/{{$hotelAndRoom2->roomimg}} "> </td>
        <td>
                <table class="table table-condensed">
                        <tr>
                            <td >Room No: {{$hotelAndRoom2->roomnumber}} </td>
                        </tr>
                        <tr>
                            <td >Type: {{$hotelAndRoom2->room_type}} </td>
                        </tr>
                        <tr>
                                <td ><strong>Cost: {{$hotelAndRoom2->price}} /Day</strong></td>
                            </tr>
                        <tr>
                        <td >Facility: {{$hotelAndRoom2->ac==1?'AC ':" "}}{{$hotelAndRoom2->fridge==1?'Fridge ':" "}}{{$hotelAndRoom2->tv==1?'Tv ':" "}}</td>
                        </tr>
                        <tr>
                                <td >Childcare:   {{$hotelAndRoom2->childcare==1?'available ':'no'}}</td>
                                </tr>



                    </table>
        </td>
        {{--Action--}}
        <td>
            <table class=" table table-condensed">
                <tr>
                <td ><a href="/user/details/{{$hotelAndRoom2->id}}" class="btn btn-primary btn-lg btn-block">Details</button></td>
                </tr>
                <tr>
                    <td ><a href="/user/details/{{$hotelAndRoom2->id}}" type="button" class="btn btn-success btn-lg btn-block">Book</button></td>
                </tr>

            </table>
        </td>
    </table>
</div>
@endforeach
@endif

{{--table two hotelAndRoom3 hotelAndRoom3  hotelAndRoom   3hotelAndRoom     3hotelAndRoom    3hotelAndRoom      3hotelAndRoom3--}}
@if($hotelAndRoom3)
@foreach ($hotelAndRoom3 as $hotelAndRoom3)
<div class="container ">
    <table class="table-custom-bordered  table-bordered col-12">
        <tr>
            <th>Hotel Image</th>
            <th>Hotel Details</th>
            <th>Room Image</th>
            <th>Hotel Details</th>
            <th>Action</th>

        </tr>

        {{--Hotel--}}
        <td><img style="height: 250px; width:250px"  class="img-fluid img-thumbnail "  src="/uploads/{{$hotelAndRoom3->hotelimg}} "></td>
        <td>
                <table class="table table-condensed">
                        <tr>
                            <td >Hotel: {{$hotelAndRoom3->hotelname}} </td>
                        </tr>
                        <tr>
                            <td >Address: {{$hotelAndRoom3->address}} </td>
                        </tr>
                        <tr>
                                <td >City: {{$hotelAndRoom3->city}} </td>
                            </tr>
                        <tr>
                            <td >Email: {{$hotelAndRoom3->email}}</td>
                        </tr>
                        <tr>
                                <td >Star: {{$hotelAndRoom3->star}}  Star</td>
                            </tr>

                    </table>
        </td>

        {{--Room--}}
        <td><img style="height: 250px; width:250px"  class="img-fluid img-thumbnail "  src="/uploads/{{$hotelAndRoom3->roomimg}} "> </td>
        <td>
                <table class="table table-condensed">
                        <tr>
                            <td >Room No: {{$hotelAndRoom3->roomnumber}} </td>
                        </tr>
                        <tr>
                            <td >Type: {{$hotelAndRoom3->room_type}} </td>
                        </tr>
                        <tr>
                                <td ><strong>Cost: {{$hotelAndRoom3->price}} /Day</strong></td>
                            </tr>
                        <tr>
                        <td >Facility: {{$hotelAndRoom3->ac==1?'AC ':" "}}{{$hotelAndRoom3->fridge==1?'Fridge ':" "}}{{$hotelAndRoom3->tv==1?'Tv ':" "}}</td>
                        </tr>
                        <tr>
                                <td >Childcare:   {{$hotelAndRoom3->childcare==1?'available ':'no'}}</td>
                                </tr>



                    </table>
        </td>
        {{--Action--}}
        <td>
            <table class=" table table-condensed">
                <tr>
                <td ><a href="/user/details/{{$hotelAndRoom3->id}}" class="btn btn-primary btn-lg btn-block">Details</button></td>
                </tr>
                <tr>
                    <td ><a href="/user/details/{{$hotelAndRoom3->id}}" type="button" class="btn btn-success btn-lg btn-block">Book</button></td>
                </tr>

            </table>
        </td>
    </table>
</div>
@endforeach
@endif


<script>

    function myfunc(){
        var x=document.getElementById("checkin").value;
        var y=document.getElementById("checkout").value;

       if(x>=y){
           alert('wrong date input');
           event.preventDefault();
       }
       else{
        form.submit();

       }
    }


</script>

@endsection
