@extends('layouts.UserLayout')
@section('content')

    <table class="table-custom-bordered  table-bordered col-12">
            <tr>
                <th>Reservation Number</th>
                <th>Room Id</th>
                <th>Check In</th>
                <th>Check in Status </th>
                <th>Check Out</th>
                <th>Check Out Status </th>
            </tr>
            @foreach ($booking as $booking)
            <tr>
                    <td> {{$booking['reservation_number']}}  </td>
                    <td>  {{$booking['roomid']}} </td>
                    <td> {{$booking['check_in_time']}}  </td>
                    <td>  {{$booking['check_in_status'] ?? 'invalid'}}</td>
                    <td> {{$booking['check_out_time']}}  </td>
                    <td> {{$booking['check_out_status'] ?? 'invalid'}}</td>
            </tr>
            @endforeach


        </table>

@endsection
