@extends('layouts.Master')

@section('content')
    <h3 style="margin: auto; width: 20%;" class="p-0">Serch Hotel</h3>
    <form class="form-group p-2" style="margin: auto; width: 20%;">

        <div class="form-group">
            <label for="location">Location</label>
            <input class="form-control" type="text" id="location" placeholder="Location"   >
        </div>
        <div class="form-group">
            <label for="location">Check in</label>
            <input class="form-control" type="date" id="date">
        </div>
        <div class="form-group">
            <label for="location">Check Out</label>
            <input class="form-control" type="date" id="date">
        </div>
        <div class="form-group">
            <label for="select">Check in</label>
            <select class="form-control " id="select">
                <option>Single</option>
                <option>Double</option>
                <option>Family</option>
                <option>4</option>
                <option>5</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    {{--Image of the Hotels--}}

    <div style="margin-top: 15px" class="container-fluid">
        <div class="row">
            @foreach($hotel as $hotel)
            <div class="col-md-3">

                <div class="card mb-3">
                    <h3 class="card-header">{{$hotel['hotelname']}}</h3>
                    <div class="card-body">
                        <h5 class="card-title">{{$hotel['hotelname']}}</h5>
                        <h6 class="card-subtitle text-muted">{{$hotel['address']}}</h6>
                    </div>
                    <img style="height: 200px; width: 100%; display: block;" src="/uploads/{{$hotel['img']}} " alt="Card image">
                    <div class="card-body">
                        <p class="card-text">{{$hotel['email']}}</p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">{{$hotel['address']}}</li>
                        <li class="list-group-item">{{$hotel['star']}} Star</li>
                    </ul>
                    <div class="card-body ">
                        <a href="/login" class="card-link">Details</a>

                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
