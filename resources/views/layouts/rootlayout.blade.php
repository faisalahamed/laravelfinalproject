<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">

    <title>@yield('title','Go Holiday')</title>
</head>

<body>
@component('component.RootMenu')
@endcomponent
@yield('content')

@component('component.footer')
@endcomponent
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>

</html>