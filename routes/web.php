<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//for  Index pages : No auth
Route::get('/', 'IndexController@index')->name('index');
Route::get('/contact', 'IndexController@contact')->name('contact');
//Route::get('/adminlogin', 'IndexController@adminlogin')->name('adminlogin');

//login and log out and signup
Auth::routes();

//for  User :
Route::get('/user/home', 'HomeController@index')->name('USER.home');
Route::get('/user/info', 'HomeController@info')->name('USER.userinfo');
Route::get('/user/bookings', 'HomeController@bookings')->name('USER.mybookings');
Route::get('/user/reserve', 'HomeController@reserve')->name('USER.reserve');
Route::get('/user/details/{id}', 'HomeController@details')->name('USER.details');
Route::post('/user/details/{id}', 'HomeController@addresrtvation');
Route::get('/user/reserve/search', 'HomeController@reserveserch')->name('USER.reserveserch');




//Admin User : Must Auth
Route::get('/adminlogin', 'Auth\AdminLoginController@loginpage')->name('ADMIN.adminlogin');
Route::post('/adminlogin', 'Auth\AdminLoginController@adminlogin');



//Admin User : Must Auth
Route::get('/admin/home', 'AdminController@index')->name('ADMIN.adminhome');
Route::get('/admin/info', 'AdminController@info')->name('ADMIN.admininfo');
Route::get('/admin/bookings', 'AdminController@bookings')->name('ADMIN.bookings');
Route::get('/admin/addroom', 'AdminController@addroom')->name('ADMIN.addroom');
Route::post('/admin/addroom', 'AdminController@addroompost')->name('ADMIN.addroom');
Route::get('/admin/roomlist', 'AdminController@roomlist')->name('ADMIN.addroom');
Route::post('/admin/adminlogout', 'AdminController@adminlogout');
Route::post('/admin/checkout', 'AdminController@checkout');



//Root User : Login

Route::get('/root', 'Auth\RootLoginController@loginpage')->name('Root.rootlogin');
Route::post('/root', 'Auth\RootLoginController@rootlogin');
//
////Root User : Must auth
Route::get('/root/home', 'RootController@index')->name('ROOT.roothome');
Route::get('/root/info', 'RootController@info')->name('ROOT.rootinfo');
Route::get('/root/addhotel', 'RootController@addhotel')->name('ROOT.addhotel');
Route::post('/root/addhotel', 'RootController@store');
Route::get('/root/adduser', 'RootController@adduser')->name('ROOT.adduser');
Route::get('/root/hotellist', 'RootController@hotellist')->name('ROOT.hotellist');
Route::post('/root/adduser', 'RootController@adduserpost');
Route::post('/root/logout', 'RootController@rootlogout');
Route::get('/root/log', 'RootController@getlog');



//Send Email

Route::post('/sendemail/send', 'SendEmailController@send');
Route::post('/sendemail/reservationMail', 'SendEmailController@reservationMail');

