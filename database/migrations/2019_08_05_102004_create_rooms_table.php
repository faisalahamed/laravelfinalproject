<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('roomnumber')->unique();
            $table->integer('hotel_id');
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('CASCADE');
            $table->string('room_type');
            $table->float('price');
            $table->integer('ac')->nullable();
            $table->integer('fridge')->nullable();
            $table->integer('tv')->nullable();
            $table->integer('childcare')->nullable();
            $table->integer('status')->nullable();
            $table->string('img')->nullable();
            $table->date('check_in')->nullable();
            $table->date('check_out')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
