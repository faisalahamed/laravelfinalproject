<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'admin',
            'email' => 'admin@xfaisal.com',
            'password' => bcrypt('admin1234'),
            'hotel_id'=>1,
        ]);
        DB::table('roots')->insert([
            'name' => 'root',
            'email' => 'root@xfaisal.com',
            'password' => bcrypt('root1234'),
        ]);
    }
}
