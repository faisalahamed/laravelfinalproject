<?php

namespace GoHoliday\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use GoHoliday\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use GoHoliday\Log;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {


        $this->middleware('guest')->except('logout');
    }
    public function logout(Request $request)
    {

      $this->logGenerator('logout','user');
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');

    }
    public function logGenerator($activity,$user_type){
        if(Auth::user()->name){
        $log=new Log();
        $log->user_name=Auth::user()->name;
        $log->email=Auth::user()->email;
        $log->user_type=$user_type;
        $log->activity=$activity;
        $log->save();
        }

    }
}
