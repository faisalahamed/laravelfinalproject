<?php

namespace GoHoliday\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GoHoliday\Log;

use  GoHoliday\Hotel;
use GoHoliday\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;

class RootController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:root');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->logGenerator('login','root');
        return view('ROOT.roothome');
    }
    public function info()
    {


        return view('ROOT.rootinfo');
    }
    public function addhotel()
    {
        return view('Root.addhotel');
    }
    public function adduser()
    {
        $hotel=Hotel::all();
        return view('root.adduser',compact('hotel'));
    }
    public function store(Request $request)
    {
        $image=$request->file('image');
        $new_name=rand().'.'.$image->getClientOriginalExtension();
        $dest=public_path('uploads');
        $image->move($dest,$new_name);

        $book=new Hotel();
        $book->hotelname=request('hotelname');
        $book->city=request('city');
        $book->address=request('address');
        $book->email=request('email');
        $book->phone=request('phone');
        $book->star=request('star');
        $book->img=$new_name;
        $book->save();
        $this->logGenerator('Add Hotel','root');
//        if($request->hasFile('image')){
//
        //    $book->update(['image'=>request()->image->store('uploads','public')]);
//
//        }
        //  dd(request('image'));
        //return view('AdminDashboard');
        return back()->with('seucces');
    }
    public function hotellist()
    {
        $hotel=Hotel::all();
        return view('ROOT.hotellist',compact('hotel'));
    }
    public function adduserpost(Request $request)
    {
        $admin=new Admin();
        $admin->name=request('name');
        $admin->email=request('email');
        $admin->password=Hash::make(request('password'));
       $admin->phone=request('name');
       $admin->hotel_id=$request->get('type');
       $admin->save();
       $this->logGenerator('User added','root');

        return back()->with('success', 'Admin Added successfully ');
    }

    public function rootlogout(Request $request)
    {

       $this->logGenerator('logout','root');
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');

    }
    public function logGenerator($activity,$user_type){
        if(Auth::user()->name){
        $log=new Log();
        $log->user_name=Auth::user()->name;
        $log->email=Auth::user()->email;
        $log->user_type=$user_type;
        $log->activity=$activity;
        $log->save();
        }
    }

    public function getlog(){
        $log=Log::all();
        return view('ROOT.log',compact('log'));

        }
}
