<?php

namespace GoHoliday\Http\Controllers;

use Illuminate\Http\Request;
use  GoHoliday\Hotel;

class IndexController extends Controller
{
    public function index(){
        $hotel=Hotel::all();
        return view('index',compact('hotel'));
}
    public function contact(){
        return view('contact');
    }
//    public function adminlogin(){
//        return view('adminlogin');
//    }
}
