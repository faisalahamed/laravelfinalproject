<?php

namespace GoHoliday\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GoHoliday\Room;
use GoHoliday\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class AdminController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->logGenerator('Login','Admin');
        return view('Admin.adminhome');
    }
    public function info()
    {
        return view('ADMIN.admininfo');
    }
    public function bookings()
    {
        //$booking=Booking::all();

        //return view('ADMIN.ourbookings',compact('booking'));
       // dd(Auth::user()->hotel_id);


       $booking=DB::table('rooms')
                    ->where('rooms.hotel_id','=',Auth::user()->hotel_id)
                    ->join('bookings','bookings.roomid','=','rooms.id')
                    ->join('users','bookings.user_id','=','users.id')

                    //->select('rooms.hotel_id')
                    ->get();
                    return view('ADMIN.ourbookings',compact('booking'));

    }
    public function addroom()
    {
        return view('ADMIN.addroom');
    }
    public function addroompost(Request $request)
    {
        $image=$request->file('image');
        $new_name=rand().'.'.$image->getClientOriginalExtension();
        $dest=public_path('uploads');
        $image->move($dest,$new_name);

        $room=new Room();
       // $room->id=Auth::user()->id ;
        $room->hotel_id=Auth::user()->hotel_id ;
        $room->roomnumber=request('roomnumber') ;
        $room->room_type=$request->input('customRadio');
        $room->price=request('price') ;
        $room->childcare=request('childcare') ;
        $room->ac=$request->input('ac');
        $room->fridge=$request->input('fridge') ;
        $room->tv=$request->input('tv') ;
        $room->img=$new_name;
        $room->save();
        $this->logGenerator('Room added','admin');
       return back()->with('success', 'Room Added successfully ');

        //dd($request->input('customRadio'));
    }
    public function roomlist()
    {
        $room=Room::where('hotel_id', '=', Auth::user()->hotel_id)->get();
        return view('ADMIN.roomlist',compact('room'));
       // dd($room);
    }
    public function adminlogout(Request $request)
    {
           // dd(Auth::Admin()->name);
      $this->logGenerator('logout','admin');
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');

    }
    public function logGenerator($activity,$user_type){

        $log=new Log();
        $log->user_name=Auth::user()->name;
        $log->email=Auth::user()->email;
        $log->user_type=$user_type;
        $log->activity=$activity;
        $log->save();
    }
    public function checkout(Request $request){

        Room::where('id', $request->checkout)->update(array('check_in' => null,'check_out' => null));
        return back();


}
}
