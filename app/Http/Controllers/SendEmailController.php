<?php

namespace GoHoliday\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use GoHoliday\Mail\SendMail2;
use GoHoliday\Mail\SendMail;



class SendEmailController extends Controller
{
    function index()
    {
        return view('send_email');
    }
    function send(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        $data = array(
            'name' => $request->name,
            'message' => $request->message,
            'email' => $request->email
        );

        Mail::to('faisal.hye@gmail.com')->send(new SendMail($data));
        return back()->with('success', 'Thanks for contacting us!');

    }
    function reservationMail($room,$email,$date)
    {
        $data = array(
            'name' => 'faisal',
            'message' => "Your Reservation is successfully done.On Room $room . On Date $date ",
            'email' => 'goholiday@gmail.com'
        );

        Mail::to($email)->send(new SendMail2($data));
        return back()->with('success', 'Thanks for contacting us!');

    }
}
