<?php

namespace GoHoliday\Http\Controllers;

use GoHoliday\Booking;
use Illuminate\Http\Request;
use GoHoliday\Hotel;
use GoHoliday\Room;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use GoHoliday\Http\Controllers\SendEmailController;
use GoHoliday\Log;
use Illuminate\Support\Carbon;


class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->logGenerator('Login','user');
        return view('USER.home');
    }
    public function info()
    {

        return view('USER.userinfo');
    }
    public function bookings()
    {

        $booking=Booking::where('user_id', '=', Auth::user()->id)->get();
        //$booking=Booking::all();
        return view('USER.mybookings',compact('booking'));
       // dd($booking);
    }
    public function reserve()
    {
        // //$hotel=Hotel::find( 2);
        // $hotel=Hotel::all();
        // return view('USER.reserve',compact('hotel'));
        // //return $hotel->rooms;
        // $room=Room::all();
        // $hotel=Hotel::all();
        // return view('USER.reserve',compact('room','hotel'));
        $hotelAndRoom2='';
        $hotelAndRoom3='';
        $hotelAndRoom=DB::table('hotels')
                        ->join('rooms','hotels.id','=','rooms.hotel_id')
                        ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                        'rooms.id','rooms.img as roomimg','rooms.roomnumber','rooms.childcare','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                        )
                        ->get();
                     // return $hotel;
                     return view('USER.reserve',compact('hotelAndRoom','hotelAndRoom2','hotelAndRoom3'));

    }
    public function details($id)
    {
        // $hotelAndRoom=Room::findOrFail($id);
        // return $hotelAndRoom;

        $hotelAndRoom=DB::table('rooms')
                    ->where('rooms.id','=',$id)
                    ->join('hotels','hotels.id','=','rooms.hotel_id')
                    ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                        'rooms.id','rooms.img as roomimg','rooms.roomnumber','rooms.childcare','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                        )
                    ->get();
                   return view('USER.details',compact('hotelAndRoom'));


    }
    public function addresrtvation($id){

         $reservation=new Booking();
         $reservation->user_id=Auth::user()->id;
         $reservation->roomid=$id;
         //$reservation->roomid=request('roomnumber');
         $reservation->check_in_time=request('check_in_date');
         $reservation->check_out_time=request('check_out_date');
         $reservation->save();
       // app('GoHoliday\Http\Controllers\SendEmailController')->reservationMail($id,Auth::user()->email,request('check_in_date'));

        Room::where('id', $id)->update(['check_in' => request('check_in_date'), 'check_out' => request('check_out_date')]);
        $this->logGenerator('Made Reservation','user');
         return back()->with('success', 'Reservation Done successfully ');
        //dd(Auth::user()->email);

    }
    public function reserveserch()
    {

       // dd(Auth::user()->maritual_status);
        $location=request('location');
        $from=request('check_in_date');
        $to=request('check_out_date');
        $hotelAndRoom='';
        $hotelAndRoom2='';
        $hotelAndRoom3='';


        if( $location){

            if(Auth::user()->maritual_status){
                if(Auth::user()->children>=1){
                    //done
                 // dd('location, married, have child');
                    $hotelAndRoom=DB::table('hotels')
                    ->join('rooms','hotels.id','=','rooms.hotel_id')
                                    ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                                    'rooms.id','rooms.childcare','rooms.img as roomimg','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                                    )
                    ->whereNull('check_out')
                    //->orwhereDate('check_out','<=',$to)
                   ->Where('hotels.city','=',$location)
                    ->Where('rooms.room_type','=','double')
                    ->Where('rooms.childcare','=',1)
                    ->get();
                    $hotelAndRoom2=DB::table('hotels')
                    ->join('rooms','hotels.id','=','rooms.hotel_id')
                                    ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                                    'rooms.id','rooms.childcare','rooms.img as roomimg','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                                    )
                    ->whereNull('check_out')
                    ->Where('rooms.room_type','=','double')
                   ->Where('hotels.city','=',$location)
                    ->whereNull('rooms.childcare')
                    ->get();


                }
                else{
                    //dd('location and married ,no child');
                    $hotelAndRoom=DB::table('hotels')
                    ->join('rooms','hotels.id','=','rooms.hotel_id')
                                    ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                                    'rooms.id','rooms.childcare','rooms.img as roomimg','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                                    )
                    ->whereNull('check_out')
                    ->whereNull('childcare')
                    ->Where('hotels.city','=',$location)
                    ->Where('rooms.room_type','=','double')
                    ->get();
                    $hotelAndRoom2=DB::table('hotels')
                    ->join('rooms','hotels.id','=','rooms.hotel_id')
                                    ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                                    'rooms.id','rooms.childcare','rooms.img as roomimg','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                                    )
                    ->whereNull('check_out')
                    ->where('childcare','=',1)
                    ->Where('hotels.city','=',$location)
                    ->Where('rooms.room_type','=','double')
                    ->get();

                }
            }
            else{
                //done
                //dd('only location single');
                $hotelAndRoom=DB::table('hotels')
            	->join('rooms','hotels.id','=','rooms.hotel_id')
                ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                        'rooms.id','rooms.img as roomimg','rooms.childcare','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                        )
                ->whereNull('check_out')
                ->Where('hotels.city','=',$location)
                ->Where('rooms.room_type','=','single')
                ->get();


                $hotelAndRoom2=DB::table('hotels')
            	->join('rooms','hotels.id','=','rooms.hotel_id')
                ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                        'rooms.id','rooms.img as roomimg','rooms.childcare','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                        )
                ->whereNull('check_out')
                ->Where('hotels.city','=',$location)
                ->Where('rooms.room_type','=','double')
                ->get();

                }
        }
        else{
            if(Auth::user()->maritual_status){
                if(Auth::user()->children>=1){
                    //done
                    //dd('no location, married, have child');
                    $hotelAndRoom=DB::table('hotels')
            	     ->join('rooms','hotels.id','=','rooms.hotel_id')
                            ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                            'rooms.id','rooms.img as roomimg','rooms.childcare','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                            )
                    ->whereNull('check_out')
                    ->Where('rooms.childcare','=',1)
                    ->Where('rooms.room_type','=','double')
                    ->get();

                     //dd('no location, married, have child');
                     $hotelAndRoom2=DB::table('hotels')
            	     ->join('rooms','hotels.id','=','rooms.hotel_id')
                            ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                            'rooms.id','rooms.img as roomimg','rooms.childcare','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                            )
                    ->whereNull('check_out')
                    ->whereNull('rooms.childcare')
                    ->Where('rooms.room_type','=','double')
                    ->get();

                }
                else{
                    //done
                    //dd('no location and married ,no child');
                    $hotelAndRoom=DB::table('hotels')
                    ->join('rooms','hotels.id','=','rooms.hotel_id')
                    ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                            'rooms.id','rooms.img as roomimg','rooms.childcare','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                            )
                    ->whereNull('rooms.check_out')
                    ->whereNull('rooms.childcare')
                    ->Where('rooms.room_type','=','double')
                    ->get();
                    $hotelAndRoom2=DB::table('hotels')
                    ->join('rooms','hotels.id','=','rooms.hotel_id')
                    ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                            'rooms.id','rooms.img as roomimg','rooms.childcare','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                            )
                    ->whereNull('rooms.check_out')
                    ->where('rooms.childcare','=',1)
                    ->Where('rooms.room_type','=','double')
                    ->get();


                }
            }
            else{
                //done
               // dd('no location single');
                $hotelAndRoom=DB::table('hotels')
                ->join('rooms','hotels.id','=','rooms.hotel_id')
                ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                        'rooms.id','rooms.img as roomimg','rooms.childcare','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                        )
                ->whereNull('check_out')
                ->Where('rooms.room_type','=','single')
                ->get();

                $hotelAndRoom2=DB::table('hotels')
                ->join('rooms','hotels.id','=','rooms.hotel_id')
                ->select('hotels.hotelname','hotels.address','hotels.email','hotels.city','hotels.phone','hotels.star','hotels.img as hotelimg',
                        'rooms.id','rooms.img as roomimg','rooms.childcare','rooms.roomnumber','rooms.room_type','rooms.price','rooms.ac','rooms.fridge','rooms.tv'
                        )
                ->whereNull('check_out')
                ->Where('rooms.room_type','=','double')
                ->get();

            }

        }
        return view('USER.reserve',compact(['hotelAndRoom','hotelAndRoom2','hotelAndRoom3']));

    }

    public function logGenerator($activity,$user_type){
        $log=new Log();
        $log->user_name=Auth::user()->name;
        $log->email=Auth::user()->email;
        $log->user_type=$user_type;
        $log->activity=$activity;
        $log->save();

    }
}
